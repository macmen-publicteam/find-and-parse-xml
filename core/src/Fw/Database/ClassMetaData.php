<?php
/**
 * Created by PhpStorm.
 * User: doba
 * Date: 15.11.16
 * Time: 08:54
 */

namespace Fw\Database;


class ClassMetaData {

	/**
	 * @var string
	 */
	private $model;

	/**
	 * @var \ReflectionClass
	 */
	private $reflectionClass;

	/**
	 * ClassMetaData constructor.
	 *
	 * @param $model
	 */
	public function __construct( $model ) {

		$this->model           = $model;
		$this->reflectionClass = new \ReflectionClass( $model );
	}


	/**
	 * @return \ReflectionClass
	 */
	public function getReflectionClass() {
		return $this->reflectionClass;
	}

	/**
	 * @return mixed
	 */
	public function getModel() {
		return $this->model;
	}


	/**
	 * @param $comment
	 *
	 * @return array|null
	 */
	protected function parseDocComment( $comment ) {
		$result = array();
		if ( preg_match_all( '/@(\w+)\s+(.*)\r?\n/m', $comment, $matches ) ) {
			$result = array_combine( $matches[1], $matches[2] );
		}

		return $result;
	}


	/**
	 * @return string
	 * @throws \LogicException
	 */
	public function getManagerObjectClassName() {

		if ( ! $this->reflectionClass->implementsInterface( ModelManagerInterface::class ) ) {
			throw new \LogicException( 'Object doesn\'t have Meta Class' );
		}

		return $this->reflectionClass->getMethod( 'getModelManager' )->invoke( $this->reflectionClass->name );
	}

	/**
	 * @return string
	 */
	public function getTableName() {
		$objectParams = $this->parseDocComment( $this->reflectionClass->getDocComment() );
		$tableName    = $this->reflectionClass->getShortName();
		if ( isset( $objectParams['name'] ) ) {
			$tableName = $objectParams['name'];
		}

		return strtolower( $tableName );
	}


}