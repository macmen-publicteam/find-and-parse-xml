<?php
/**
 * Created by PhpStorm.
 * User: doba
 * Date: 16.11.16
 * Time: 10:18
 */

namespace Fw\Database;


class DataGrid {

	/**
	 * @var array
	 */
	private $items;


	/**
	 * @var PropertyAccess
	 */
	private $propertyAccess;
	/**
	 * @var array
	 */
	private $fields;

	/**
	 * @var ClassMetaData
	 */
	private $classMetaData;

	public function __construct( array $items, array $fields, ClassMetaData $classMetaData ) {

		$this->items          = $items;
		$this->propertyAccess = new PropertyAccess();
		$this->fields         = $fields;
		$this->classMetaData  = $classMetaData;
	}


	/**
	 * @return array
	 */
	public function getRows() {
		$result         = array();
		$objectInstance = $this->classMetaData->getReflectionClass()->newInstance();
		if ( 0 < count( $this->items ) ) {
			foreach ( $this->items as $item ) {
				$object = clone $objectInstance;
				foreach ( $this->fields as $field ) {
					if ( isset( $item[ $field ] ) ) {
						$this->propertyAccess->setValue( $object, $field, $item[ $field ] );
					}
				}
				$result[] = $object;
			}
		}

		return $result;
	}

	public function getSingleResult() {
		$items = $this->getRows();
		if ( ! $items ) {
			return null;
		}
		$item = array_shift( $items );

		return $item;
	}

}