<?php
/**
 * Created by PhpStorm.
 * User: doba
 * Date: 14.11.16
 * Time: 13:06
 */

namespace Fw\Database;


class Connection {

	/**
	 * @var \PDOStatement
	 */
	private $stmt = null;
	private $params = array();
	private $sql;

	/**
	 * @var \PDO
	 */
	private $pdoConnection;

	public function __construct( array $databaseConfiguration ) {
		$databaseName     = isset( $databaseConfiguration['name'] ) ? $databaseConfiguration['name'] : null;
		$databaseHost     = isset( $databaseConfiguration['host'] ) ? $databaseConfiguration['host'] : null;
		$databaseUsername = isset( $databaseConfiguration['username'] ) ? $databaseConfiguration['username'] : null;
		$databasePassword = isset( $databaseConfiguration['password'] ) ? $databaseConfiguration['password'] : null;
		$databaseOptions  = isset( $databaseConfiguration['options'] ) ? $databaseConfiguration['options'] : array();

		if ( ! $databaseName || ! $databaseHost || ! $databaseUsername || is_null( $databasePassword ) ) {
			throw new \LogicException( 'The DataBase configuration is not correct. Please Check your settings in and try again' );
		}
		$this->pdoConnection = new \PDO( "pgsql:dbname=$databaseName;host=$databaseHost", $databaseUsername, $databasePassword, $databaseOptions );

	}

	/**
	 * @param       $sql
	 * @param array $params
	 * @param array $pdoOptions
	 *
	 * @return $this
	 */
	public function prepare( $sql, $params = array(), $pdoOptions = array() ) {
		$this->stmt   = $this->pdoConnection->prepare( $sql, $pdoOptions );
		$this->params = $params;
		$this->sql    = $sql;

		return $this;
	}

	/**
	 *
	 * @param $params
	 *
	 * @return $this;
	 */
	public function execute( $params = array() ) {
		if ( $params ) {
			$this->params = $params;
		}
		$this->stmt->execute( $this->params );

		return $this;
	}

	/**
	 * @return array
	 */
	public function fetchAllAssoc() {
		return $this->stmt->fetchAll( \PDO::FETCH_ASSOC );
	}

	/**
	 * @return mixed
	 */
	public function fetchRowAssoc() {
		return $this->stmt->fetch( \PDO::FETCH_ASSOC );
	}

	/**
	 * @return array
	 */
	public function fetchAllNum() {
		return $this->stmt->fetchAll( \PDO::FETCH_NUM );
	}

	/**
	 * @return mixed
	 */
	public function fetchRowNum() {
		return $this->stmt->fetch( \PDO::FETCH_NUM );
	}

	/**
	 * @return array
	 */
	public function fetchAllObj() {
		return $this->stmt->fetchAll( \PDO::FETCH_OBJ );
	}

	/**
	 * @return mixed
	 */
	public function fetchRowObj() {
		return $this->stmt->fetch( \PDO::FETCH_OBJ );
	}

	public function fetchAllColumn( $column ) {
		return $this->stmt->fetchAll( \PDO::FETCH_COLUMN, $column );
	}

	/**
	 * @param $column
	 *
	 * @return mixed
	 */
	public function fetchRowColumn( $column ) {
		return $this->stmt->fetch( \PDO::FETCH_BOUND, $column );
	}

	/**
	 * @param $class
	 *
	 * @return array
	 */
	public function fetchAllClass( $class ) {
		return $this->stmt->fetchAll( \PDO::FETCH_CLASS, $class );
	}

	/**
	 * @param $class
	 *
	 * @return mixed
	 */
	public function fetchRowClass( $class ) {
		$this->stmt->setFetchMode( \PDO::FETCH_CLASS, $class );

		return $this->stmt->fetch();
	}

	/**
	 * @return string
	 */
	public function getLastInsertId() {
		return $this->pdoConnection->lastInsertId();
	}

	/**
	 * @return int
	 */
	public function getAffectedRows() {
		return $this->stmt->rowCount();
	}

	/**
	 * @return \PDOStatement
	 */
	public function getSTMT() {
		return $this->stmt;
	}

	/**
	 * @return array
	 */
	public function getColumns() {
		$columns = array();

		if ( 0 < $this->stmt->columnCount() ) {
			for ( $i = 0; $i < $this->stmt->columnCount(); $i ++ ) {
				$columnMeta = $this->stmt->getColumnMeta( $i );
				if ( isset( $columnMeta['name'] ) ) {
					$columns[] = $columnMeta['name'];
				}
			}
		}

		return $columns;
	}


	/**
	 * @return \PDO
	 */
	public function getPdoConnection() {
		return $this->pdoConnection;
	}
}