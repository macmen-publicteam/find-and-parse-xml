<?php
/**
 * Created by PhpStorm.
 * User: doba
 * Date: 14.11.16
 * Time: 13:05
 */

namespace Fw\Database;


class ManageBuilder {

	/**
	 * @var Connection
	 */
	private $connection;

	/**
	 * @var string
	 */
	private $modelName;


	/**
	 * @var ClassMetaData
	 */
	private $classMeta;

	public function __construct( Connection $connection, $modelName ) {

		$this->connection = $connection;
		$this->modelName  = $modelName;
		$this->classMeta  = new ClassMetaData( $this->modelName );

	}


	/**
	 * @return  Manager
	 * @throws \ReflectionException
	 */
	public function getManager() {

		try {
			$class = $this->classMeta->getManagerObjectClassName();
		} catch ( \LogicException $exception ) {
			$class = Manager::class;
		}

		$reflection = new \ReflectionClass( $class );

		return $reflection->newInstance( $this->connection, $this->classMeta );
	}

}