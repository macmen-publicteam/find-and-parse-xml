<?php
/**
 * Created by PhpStorm.
 * User: doba
 * Date: 14.11.16
 * Time: 13:03
 */

namespace Fw\Database;


interface ModelManagerInterface {

	/**
	 * @return string
	 */
	public static function getModelManager();
}