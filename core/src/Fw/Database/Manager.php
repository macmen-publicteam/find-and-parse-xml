<?php
/**
 * Created by PhpStorm.
 * User: doba
 * Date: 13.11.16
 * Time: 14:30
 */

namespace Fw\Database;


use Fw\Database\Exception\ResultNotFoundException;

class Manager {

	/**
	 * @var Connection
	 */
	protected $connection;

	/**
	 * @var ClassMetaData
	 */
	private $classMetaData;

	/**
	 * Manager constructor.
	 *
	 * @param Connection    $connection
	 * @param ClassMetaData $classMetaData
	 *
	 */
	public function __construct( Connection $connection, ClassMetaData $classMetaData ) {

		$this->connection    = $connection;
		$this->classMetaData = $classMetaData;
	}


	/**
	 * @return Connection
	 */
	public function getConnection() {
		return $this->connection;
	}

	/**
	 * @return ClassMetaData
	 */
	public function getClassMetaData() {
		return $this->classMetaData;
	}


	/**
	 * @param array $fields
	 *
	 * @return InsertQueryBuilder
	 */
	public function createInsertQueryBuilder( array $fields ) {
		return new InsertQueryBuilder( $this, $fields );
	}

	/**
	 * @param array  $fields
	 * @param string $separator
	 *
	 * @return string
	 */
	public function prepareSqlFieldAsString( array $fields, $separator = ',' ) {
		return implode( $separator, $fields );
	}

	/**
	 * @param array $params
	 *
	 * @return array
	 */
	public function prepareExecuteValue( array $params ) {
		$fields = array();
		foreach ( $params as $fieldName => $value ) {
			$fields[ sprintf( ':%s', $fieldName ) ] = $value;
		}

		return $fields;
	}


	public function prepareExecuteValueFromList( array $fields ) {
		$fields = array_flip( $fields );

		return array_keys( $this->prepareExecuteValue( $fields ) );
	}

	/**
	 * @param array  $fields
	 * @param string $separator
	 *
	 * @return string
	 */
	public function prepareActionClause( array $fields, $separator = ' AND ' ) {

		return implode( $separator, array_map( function ( $fieldName ) {
			return $fieldName . ' = :' . $fieldName;
		}, $fields ) );
	}

	/**
	 * @param array $params
	 * @param array $selectFields
	 *
	 * @throws ResultNotFoundException
	 * @return object
	 */
	public function getWithAndStatement( array $params, array $selectFields = array( "*" ) ) {
		$fields = array_keys( $params );

		$executeValues = $this->prepareExecuteValue( $params );

		$where = $this->prepareActionClause( $fields );


		$sql = sprintf( 'SELECT %s FROM %s WHERE %s LIMIT 1;', $this->prepareSqlFieldAsString( $selectFields ), $this->classMetaData->getTableName(), $where );
		$this->connection->prepare( $sql );

		$object = $this->connection->execute( $executeValues );
		if ( ! $object ) {
			throw new ResultNotFoundException( sprintf( 'Selection with values %s not found', implode( ',', $executeValues ) ) );
		}

		return $this->getDataGrid()->getSingleResult();
	}


	protected function getDataGrid() {
		return new DataGrid($this->connection->fetchAllAssoc(),$this->connection->getColumns(),$this->classMetaData);
	}

	/**
	 * @param array $updateParams
	 * @param array $whereParams
	 *
	 * @return mixed
	 */
	public function updateWithAndStatement( array $updateParams, array  $whereParams ) {
		$updateKeys    = array_keys( $updateParams );
		$whereKeys     = array_keys( $whereParams );
		$executeValues = $this->prepareExecuteValue( array_merge( $updateParams, $whereParams ) );

		$sql = sprintf( 'UPDATE %s SET %s WHERE %s;',
			$this->classMetaData->getTableName(),
			$this->prepareActionClause( $updateKeys ),
			$this->prepareActionClause( $whereKeys )
		);
		$this->connection->prepare( $sql );
		$this->connection->execute( $executeValues );
		return $this->getDataGrid()->getSingleResult();
	}

	/**
	 * @return array
	 */
	protected function getResult() {
		return $this->getDataGrid()->getRows();
	}

}