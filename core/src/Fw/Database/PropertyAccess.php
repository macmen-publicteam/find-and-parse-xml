<?php
/**
 * Created by PhpStorm.
 * User: doba
 * Date: 16.11.16
 * Time: 09:52
 */

namespace Fw\Database;


class PropertyAccess {


	public function setValue(&$object, $property, $value ) {


		$property = lcfirst( $this->camelize( $property ) );

		$setter     = 'set' . $property;
		$reflection = new \ReflectionClass( $object );

		if ( $this->isMethodAccessible( $reflection, $setter, 1 ) ) {

			$method = $reflection->getMethod( $setter );
			if ( $propertyClass = $this->getPropertyClass( $method ,$value) ) {
				$method->invoke($object,$propertyClass);
			}else{

				$method->invoke($object,$value);
			}

		}

		return $object;
	}


	/**
	 * Returns whether a method is public and has the number of required parameters.
	 *
	 * @param \ReflectionClass $class The class of the method
	 * @param string           $methodName The method name
	 * @param int              $parameters The number of parameters
	 *
	 * @return bool Whether the method is public and has $parameters required parameters
	 */
	private function isMethodAccessible( \ReflectionClass $class, $methodName, $parameters ) {
		if ( $class->hasMethod( $methodName ) ) {
			$method = $class->getMethod( $methodName );
			if ( $method->isPublic()
			     && $method->getNumberOfRequiredParameters() <= $parameters
			     && $method->getNumberOfParameters() >= $parameters
			) {
				return true;
			}
		}

		return false;
	}


	/**
	 * Camelizes a given string.
	 *
	 * @param string $string Some string
	 *
	 * @return string The camelized version of the string
	 */
	private function camelize( $string ) {
		return str_replace( ' ', '', ucwords( str_replace( '_', ' ', $string ) ) );
	}

	private function getPropertyClass( \ReflectionMethod $method, $value ) {
		if ( 0 === $method->getNumberOfParameters() ) {
			return null;
		}
		foreach ( $method->getParameters() as $parameter ) {
			if ( $class = $parameter->getClass() ) {
				return $class->newInstance( $value );
			}
		}

		return null;
	}
}