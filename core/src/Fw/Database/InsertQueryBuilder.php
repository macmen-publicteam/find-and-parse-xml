<?php
/**
 * Created by PhpStorm.
 * User: doba
 * Date: 15.11.16
 * Time: 09:42
 */

namespace Fw\Database;


class InsertQueryBuilder {

	/**
	 * @var Connection
	 */
	private $connection;

	/**
	 * @var array
	 */
	private $fields;

	/**
	 * @var array
	 */
	private $insertCollection = array();

	/**
	 * @var ClassMetaData
	 */
	private $classMetaData;

	/**
	 * @var Manager
	 */
	private $manager;

	/**
	 * @var array
	 */
	private $insertConflictFields = array();


	/**
	 * @var array
	 */
	private $updateConflictField = array();

	/**
	 * InsertQueryBuilder constructor.
	 *
	 * @param Manager $manager
	 * @param array   $fields
	 */
	public function __construct( Manager $manager, array $fields ) {
		$this->manager       = $manager;
		$this->fields        = $fields;
		$this->classMetaData = $this->manager->getClassMetaData();
		$this->connection    = $this->manager->getConnection();
	}

	/**
	 *
	 * @param array $insertValues
	 *
	 * @return $this
	 */
	public function addValue( array $insertValues ) {
		$keys = array_keys( $insertValues );
		if ( ! array_diff( $keys, $this->fields ) ) {
			$this->insertCollection[] = $insertValues;
		}

		return $this;
	}

	/**
	 * @param array $fields
	 *
	 * @return $this
	 */
	public function addConflictFields( array $fields ) {
		$this->insertConflictFields = $fields;

		return $this;
	}

	public function setUpdateConflictField( $field, $value ) {
		$this->updateConflictField = array(
			'key'   => $field,
			'value' => $value
		);

		return $this;
	}

	/**
	 *
	 */
	public function execute() {

		if ( 0 === count( $this->insertCollection ) ) {
			throw new \LogicException( 'Add Insert Values' );
		}

		$sql = sprintf( 'INSERT INTO %s (%s) VALUES (%s) ',
			$this->classMetaData->getTableName(),
			$this->manager->prepareSqlFieldAsString( $this->fields ),
			$this->manager->prepareSqlFieldAsString( $this->manager->prepareExecuteValueFromList( $this->fields ) )
		);

		if ( 0 < count( $this->insertConflictFields ) ) {
			$sql .= sprintf( 'ON CONFLICT (%s) ', $this->manager->prepareSqlFieldAsString( $this->insertConflictFields ) );

			if ( 0 === count( $this->updateConflictField ) ) {
				$sql .= "DO NOTHING";
			} else {

				$sql .= sprintf( "DO UPDATE SET %s = '%s'", $this->updateConflictField['key'], $this->updateConflictField['value'] );
			}
		}

		$this->connection->prepare( $sql );
		$this->connection->getPdoConnection()->beginTransaction();

		foreach ( $this->insertCollection as $value ) {
			try {
				$this->connection->execute( $value );
			} catch ( \PDOException $exception ) {
				throw new \LogicException( $exception->getMessage() );
			}
		}

		$this->connection->getPdoConnection()->commit();

		return true;
	}

}