<?php
/**
 * Created by PhpStorm.
 * User: doba
 * Date: 12.11.16
 * Time: 19:46
 */

namespace Fw\DomCrawler;


class XmlCrawler {


	/**
	 * @param  $content
	 * @return \DOMDocument
	 */
	protected function getDomDocument( $content ) {
		if ( ! preg_match( '/xmlns:/', $content ) ) {
			$content = str_replace( 'xmlns', 'ns', $content );
		}

		$internalErrors  = libxml_use_internal_errors( true );
		$disableEntities = libxml_disable_entity_loader( true );

		$dom                  = new \DOMDocument( '1.0', 'UTF-8' );
		$dom->validateOnParse = true;

		if ( '' !== trim( $content ) ) {
			@$dom->loadXML( $content );
		}

		libxml_use_internal_errors( $internalErrors );
		libxml_disable_entity_loader( $disableEntities );

		return $dom;
	}

	/**
	 * @param \SplFileInfo $file
	 *
	 * @return \DOMDocument
	 */
	protected function getDomDocumentByFile(\SplFileInfo $file) {

		$content = file_get_contents( $file->getRealPath() );
		return $this->getDomDocument($content);
	}


	/**
	 * @param \DOMElement $domElement
	 * @param array       $tags
	 *
	 * @return array
	 */
	protected function getTextContentAndConvertDomElementToArray( \DOMElement $domElement, array $tags ) {
		$result = array();
		foreach ( $tags as $row ) {
			$element     = $domElement->getElementsByTagName( $row );
			$textContent = null;

			if ( 0 < $element->length ) {
				$textContent = $element->item( 0 )->textContent;
			}

			$result[ $row ] = $textContent;
		}

		return $result;
	}
}