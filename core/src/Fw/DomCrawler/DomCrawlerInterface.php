<?php
/**
 * Created by PhpStorm.
 * User: doba
 * Date: 13.11.16
 * Time: 10:56
 */

namespace Fw\DomCrawler;


interface DomCrawlerInterface {

	/**
	 * @return array
	 */
	public function getItems();
}