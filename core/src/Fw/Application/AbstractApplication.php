<?php
/**
 * Created by PhpStorm.
 * User: doba
 * Date: 12.11.16
 * Time: 12:27
 */

namespace Fw\Application;

use Fw\Container;

abstract class AbstractApplication implements ApplicationInterface {

	/**
	 * @var Container
	 */
	protected $container;


	/**
	 * @param Container $container
	 */
	public function setContainer(Container $container ) {
		$this->container = $container;
	}

	/**
	 * @return Container
	 */
	public function getContainer() {
		return $this->container;
	}

	/**
	 * @param Container $container
	 */
	public function beforeRun( Container $container ) {}
}

