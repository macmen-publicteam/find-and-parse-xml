<?php
/**
 * Created by PhpStorm.
 * User: doba
 * Date: 12.11.16
 * Time: 10:01
 */

namespace Fw\Application;


use Fw\Container;

interface ApplicationInterface {

	/**
	 * Execute All Application Staff
	 */
	public function run();

	/**
	 * @param Container $application
	 *
	 */
	public function setContainer(Container $application);

	/**
	 * @param Container $container
	 */
	public function beforeRun(Container $container);
}