<?php
/**
 * Created by PhpStorm.
 * User: doba
 * Date: 12.11.16
 * Time: 10:00
 */

namespace Fw\Command;

use Fw\Application\AbstractApplication;
use Fw\Command\Exception\CommandNotFoundException;
use Fw\Filesystem\Finder;

class CommandApplication extends AbstractApplication {

	/**
	 * @var \Fw\Command\Input
	 */
	private $input;

	/**
	 * @var Output
	 */
	private $output;

	public function __construct() {
		$this->input  = new Input();
		$this->output = new Output();
	}


	/**
	 * Execute All Application Staff
	 */
	public function run() {
		$this->input->parse();
		$commandBuilder = new CommandBuilder(new Finder($this->container->getCommandFolder()));
		$printMethods = array();
		if($this->input->getMethod()) {
			try{
				$command = $commandBuilder->get($this->input->getMethod());
				$command->setContainer($this->container);
				$command->execute($this->input,$this->output);
			}catch (CommandNotFoundException $exception) {
				$this->output->printError($exception->getMessage());
			}
			return true;
		}

		foreach ( (array) $commandBuilder->getCommands() as $name => $registerCommand ) {
			$printMethods[ $name ] = sprintf( 'Command: %s', $registerCommand->getName() );

			if ( method_exists( $registerCommand, '__toString' ) ) {
				$printMethods[ $name ] .= sprintf( '%s ---- Description: %s', PHP_EOL, $registerCommand->__toString() );
			}
		}

		return $this->output->printSuccess( implode( PHP_EOL, $printMethods ) );
	}



}