<?php
/**
 * Created by PhpStorm.
 * User: doba
 * Date: 29.08.16
 * Time: 10:40
 */

namespace Fw\Command;

use Fw\Command\Exception\CommandException;
use Fw\Command\Exception\CommandExceptionInterface;
use Fw\Command\Exception\CommandParameterNotFoundException;
use Fw\Container;

abstract class BaseCommand implements CommandInterface {

	/**
	 * @var Container
	 */
	protected $container;


	/**
	 * @param Container $container
	 */
	public function setContainer( Container $container ) {
		$this->container = $container;
	}

	/**
	 * @return Container
	 */
	public function getContainer() {
		return $this->container;
	}


	/**
	 * @param             $message
	 * @param Output|null $output
	 *
	 * @return CommandParameterNotFoundException
	 */
	protected function createParameterNotFoundException( $message, Output $output = null ) {

		return $this->createCommandException( $message, CommandParameterNotFoundException::class, $output );
	}

	/**
	 * @param             $message
	 * @param             $exception
	 * @param Output|null $output
	 * @throws \LogicException
	 * @return \LogicException|CommandExceptionInterface
	 */
	protected function createCommandException( $message, $exception = CommandException::class, Output $output = null ) {

		$reflection = new \ReflectionClass( $exception );

		if ( ! $reflection->implementsInterface( CommandExceptionInterface::class ) ) {
			throw new \LogicException( sprintf( sprintf( 'The class must implement %s', CommandExceptionInterface::class ) ) );
		}

		if ( is_null( $output ) ) {
			$output = new Output();
		}

		return $reflection->newInstance( $output, $message );
	}
}