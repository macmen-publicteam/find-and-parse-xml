<?php

/**
 * Created by PhpStorm.
 * User: doba
 * Date: 29.08.16
 * Time: 10:24
 */
namespace Fw\Command;


use Fw\Container;

interface CommandInterface {

	/**
	 * @param Input  $input
	 * @param Output $output
	 *
	 * @return void
	 */
	public function execute( Input $input, Output $output );

	/**
	 * @return string
	 */
	public function getName();

	/**
	 * @param Container $container
	 */
	public function setContainer( Container $container );

}