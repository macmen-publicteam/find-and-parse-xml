<?php
/**
 * Created by PhpStorm.
 * User: doba
 * Date: 12.11.16
 * Time: 17:08
 */

namespace Fw\Command\Exception;

interface CommandExceptionInterface {

	/**
	 * @return string
	 */
	public function getOutPutMessage();

}