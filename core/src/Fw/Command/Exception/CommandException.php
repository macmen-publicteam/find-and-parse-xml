<?php
/**
 * Created by PhpStorm.
 * User: doba
 * Date: 12.11.16
 * Time: 17:10
 */

namespace Fw\Command\Exception;


use Exception;
use Fw\Command\Output;

class CommandException extends \LogicException implements CommandExceptionInterface {

	/**
	 * @var Output
	 */
	private $output;

	/**
	 * CommandException constructor.
	 *
	 * @param Output         $input
	 * @param int            $message
	 * @param null           $code
	 * @param Exception|null $previous
	 */
	public function __construct( Output $input, $message, $code = null, Exception $previous = null ) {
		parent::__construct( $message, 500, $previous );
		$this->output = $input;
	}

	/**
	 * @return string
	 */
	public function getOutPutMessage() {
		return $this->output->printError( $this->getMessage() );
	}
}