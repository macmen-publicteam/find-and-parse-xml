<?php
/**
 * Created by PhpStorm.
 * User: doba
 * Date: 27.4.2016 г.
 * Time: 11:26
 */

namespace Fw\Command;


class Input {

	private $tokens = array();
	private $parsed = array();
	private $method = null;

	public function __construct( array $argv = null ) {
		if ( null === $argv ) {
			$argv = $_SERVER['argv'];
		}
		// strip the application name
		array_shift( $argv );
		if ( isset( $argv[0] ) ) {
			$this->method = $argv[0];
			unset( $argv[0] );

		}
		$this->tokens = $argv;
	}

	public function parse() {
		$parseOptions = true;
		while ( null !== $token = array_shift( $this->tokens ) ) {
			if ( $parseOptions && '--' == $token ) {
				$parseOptions = false;
			} elseif ( $parseOptions && 0 === strpos( $token, '--' ) ) {
				$this->parseLongOption( $token );
			}
		}
	}

	private function parseLongOption( $token ) {
		$name = substr($token, 2);
		$value = null;
		if(false === strpos($name, '=')) {
			if(count($this->tokens) > 0) {
				$value = array_shift($this->tokens);
			}
		}else {
			$pos = strpos($name, '=');
			$value = substr($name, $pos + 1);
			$name = substr($name, 0, $pos);
		}
		$this->addArgument($name,$value);
	}

	/**
	 * @param $name
	 * @param $value
	 * @throws \InvalidArgumentException
	 * @return $this
	 */
	public function addArgument( $name, $value ) {
		if ( $this->has( $name ) ) {
			throw new \InvalidArgumentException( sprintf( 'The "--%s" option already exist.', $name ) );
		}
		$this->parsed[ $name ] = $value;

		return $this;
	}

	public function getArgvList() {
		return $this->parsed;
	}

	public function getMethod() {
		return $this->method;
	}

	public function has( $name ) {
		return array_key_exists( $name, $this->parsed );
	}

	public function get( $name, $default = null ) {
		return $this->has( $name ) ? $this->parsed[ $name ] : $default;
	}

	public function getInt( $name ) {
		return (int) $this->get( $name );
	}

	public function getBool( $name ) {
		$value = $this->get( $name );
		if ( ! $value ) {
			return false;
		}
		$value = strtolower( $value );
		if ( $value === 'true' ) {
			return true;
		}
		if ( $value == 'false' ) {
			return false;
		}

		return (bool) $value;
	}
}