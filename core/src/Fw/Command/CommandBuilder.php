<?php
/**
 * Created by PhpStorm.
 * User: doba
 * Date: 11.11.16
 * Time: 16:52
 */

namespace Fw\Command;


use Command\ParseXmlCommand;
use Fw\Command\Exception\CommandNotFoundException;
use Fw\Filesystem\Finder;

class CommandBuilder {

	private $commands = array();

	/**
	 * @var Finder
	 */
	private $finder;

	public function __construct( Finder $finder ) {
		$this->finder = $finder;
	}


	/**
	 * @return array
	 */
	public function getCommands() {
		if ( ! $this->commands ) {
			$p     = function ( \SplFileInfo $file ) {
				if ( preg_match( '/Command.php$/', $file->getFilename() ) ) {
					return true;
				}

				return false;
			};
			$files = $this->finder->find( $p );
			if ( $files ) {
				foreach ( $files as $file ) {
					/**
					 * @var $file \SplFileInfo
					 */
					$class = 'Command' . '\\' . $file->getBasename( '.php' );
					$ref   = new \ReflectionClass( $class );
					if ( $ref->implementsInterface( CommandInterface::class ) && ! $ref->isAbstract() ) {
						/**
						 * @$object CommandInterface
						 */
						$object = $ref->newInstance();
						if ( $object->getName() ) {
							$this->commands[ $object->getName() ] = $object;
						}
					}
				}
			}
		}

		return $this->commands;
	}


	/**
	 * @param $commandName
	 *
	 * @throws CommandNotFoundException
	 * @return BaseCommand
	 */
	public function get( $commandName ) {
		$commands = $this->getCommands();
		if ( isset( $commands[ $commandName ] ) ) {
			return $commands[ $commandName ];
		}
		throw new CommandNotFoundException( new Output(), sprintf( 'Command with name "%s" not found', $commandName ) );
	}

}
