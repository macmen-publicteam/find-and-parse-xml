<?php
/**
 * Created by PhpStorm.
 * User: doba
 * Date: 27.4.2016 г.
 * Time: 11:26
 */

namespace Fw\Command;


class Output {

	static $styles = array(
		'success' => "\033[0;32m%s\033[0m",
		'error'   => "\033[31;31m%s\033[0m",
		'info'    => "\033[33;33m%s\033[0m"
	);

	protected function printConsole( $text, $color = null, $newLine = true ) {
		$format = '%s';

		if ( isset( self::$styles[ $color ] ) ) {
			$format = self::$styles[ $color ];
		}

		if ( $newLine ) {
			$format .= PHP_EOL;
		}

		return printf( $format, $text );
	}

	public function printError( $text ) {
		return $this->printConsole( $text, 'error' );
	}

	public function printSuccess( $text ) {
		return $this->printConsole( $text, 'success' );
	}

	public function printInfo( $text ) {
		return $this->printConsole( $text, 'info' );
	}

}