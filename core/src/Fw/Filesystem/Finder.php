<?php
/**
 * Created by PhpStorm.
 * User: doba
 * Date: 29.08.16
 * Time: 11:01
 */

namespace Fw\Filesystem;


use Fw\Filesystem\Exception\DirectoryNotFoundException;
use Fw\Filesystem\Exception\FileNotFoundException;

class Finder {


	private $directory = null;

	public function __construct( $directory ) {

		$this->directory = realpath( $directory );
		if ( ! $this->directory ) {
			throw new DirectoryNotFoundException( sprintf( 'Directory with path: %s not found', $directory ) );
		}
	}


	/**
	 * @return \SplFileInfo[]
	 */
	public function getFiles() {
		return self::in( $this->directory );
	}

	/**
	 * @param \Closure $closure
	 *
	 * @return \SplFileInfo[]
	 */
	public function find( \Closure $closure ) {
		return self::findSpecificFiles( $this->directory, $closure );
	}

	/**
	 * @param $dir
	 *
	 * @return array
	 */
	static function in( $dir ) {
		$result = array();
		if ( ! is_dir( $dir ) ) {
			throw new \InvalidArgumentException( sprintf( 'The "%s" directory does not exist.', $dir ) );
		}
		$files = array_diff( scandir( $dir ), array( '.', '..' ) );
		if ( count( $files ) > 0 ) {
			foreach ( $files as $file ) {
				$result[] = new \SplFileInfo( $dir . DIRECTORY_SEPARATOR . $file );
			}
		}

		return $result;
	}


	static function findSpecificFiles( $dir, \Closure $closure ) {
		$files  = self::in( $dir );
		$result = array();
		if ( $files ) {
			foreach ( $files as $key => $file ) {
				if ( $closure( $file, $key ) ) {
					$result[ $key ] = $file;
				}
			}
		}

		return $result;
	}

	/**
	 * @param $file
	 * @throws FileNotFoundException
	 * @return string
	 */
	public function getFile( $file ) {
		if ( $filePath = realpath( $this->directory . DIRECTORY_SEPARATOR . $file ) ) {
			return $filePath;
		}

		throw new FileNotFoundException( sprintf('File %s not Found',$this->directory.DIRECTORY_SEPARATOR.$file) );
	}

}