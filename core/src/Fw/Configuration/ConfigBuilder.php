<?php
/**
 * Created by PhpStorm.
 * User: doba
 * Date: 11.11.16
 * Time: 16:14
 */

namespace Fw\Configuration;


use Fw\Filesystem\Finder;

class ConfigBuilder {

	/**
	 * @var Finder
	 */
	private $finder;

	/**
	 * ConfigBuilder constructor.
	 *
	 * @param Finder $finder
	 */
	public function __construct( Finder $finder) {
		$this->finder = $finder;
	}

	public function buildConfiguration( Configuration $configuration) {
		$files = $this->finder->getFiles();
		if ( count( $files ) > 0 ) {
			foreach ( $files as $file ) {
				/**
				 * @var $file \SplFileInfo
				 */
				$fileData = include_once $file->getRealPath();
				if ( $fileData && is_array( $fileData ) ) {
					$configuration->add( $file->getBasename( '.php' ), $fileData );
				}
			}
		}
	}
}