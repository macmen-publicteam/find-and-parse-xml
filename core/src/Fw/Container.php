<?php

/**
 * Created by PhpStorm.
 * User: Dobromir Ivanov
 * Date: 29.08.16
 * Description of Loader
 * @author Dobromir Ivanov
 */

namespace Fw;

use Fw\Application\ApplicationInterface;
use Fw\Command\Exception\CommandExceptionInterface;
use Fw\Configuration\ConfigBuilder;
use Fw\Configuration\Configuration;
use Fw\Database\Connection;
use Fw\Database\ManageBuilder;
use Fw\Database\Manager;
use Fw\Filesystem\Finder;

class Container {

	/**
	 * @var $configuration Configuration
	 */
	private $configuration;

	/**
	 * @var Manager
	 */
	private $dataBaseManager = null;

	/**
	 * @var Template\Template
	 */
	private $template;

	public function __construct() {
		set_exception_handler( array( $this, '_exceptionHandler' ) );
		$this->configuration = new Configuration();
		$this->template = new Template\Template();
	}


	public function run( ApplicationInterface $application ) {
		$configBuilder = new ConfigBuilder( new Finder( $this->getConfigurationFolder() ) );
		$configBuilder->buildConfiguration( $this->configuration );
		$application->beforeRun($this);
		$application->setContainer( $this );
		$application->run();
	}


	/**
	 * {@inheritdoc}
	 *
	 * @api
	 */
	public function getRootDir() {
		return __DIR__;
	}


	public function getAppFolder() {
		return realpath( sprintf( '%s/../../../app', $this->getRootDir() ) );
	}

	public function getConfigurationFolder() {
		return sprintf( '%s/Configuration', $this->getAppFolder() );
	}

	public function getCommandFolder() {
		return sprintf( '%s/Command', $this->getAppFolder() );
	}

	/**
	 * @return Configuration
	 */
	public function getConfiguration() {
		return $this->configuration;
	}

	public function _exceptionHandler( \Throwable $exception ) {

		if ( $exception instanceof CommandExceptionInterface ) {
			return $exception->getOutPutMessage();
		}


		echo $exception->getMessage().PHP_EOL;

	}

	public function getDatabaseManager($model) {
		if ( null === $this->dataBaseManager ) {
			$databaseConfiguration = $this->configuration->get('database');
			$modelBuilder = new ManageBuilder(new Connection($databaseConfiguration),$model);
			$this->dataBaseManager = $modelBuilder->getManager();
		}

		return $this->dataBaseManager;
	}

	/**
	 * @return Template\Template
	 */
	public function getTemplate() {
		return $this->template;
	}

	/**
	 * @param Template\Template $template
	 *
	 * @return $this;
	 */
	public function setTemplate( $template ) {
		$this->template = $template;

		return $this;
	}

}