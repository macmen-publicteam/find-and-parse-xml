<?php
/**
 * Created by PhpStorm.
 * User: doba
 * Date: 15.11.16
 * Time: 19:09
 */

namespace Fw\Controller;


use Fw\Container;

abstract class BaseController {

	/**
	 * @var Container
	 */
	protected $container;

	/**
	 * @param Container $container
	 *
	 * @return $this
	 */
	public function setContainer( Container $container ) {
		$this->container = $container;

		return $this;
	}


	/**
	 * @param $model
	 *
	 * @return \Fw\Database\Manager
	 */
	public function getDatabase( $model ) {
		return $this->container->getDatabaseManager( $model );
	}


	/**
	 * @param       $template
	 * @param array $data
	 *
	 * @return string
	 */
	public function render( $template, array $data = array() ) {
		return $this->container->getTemplate()->render( $template, $data );
	}


}