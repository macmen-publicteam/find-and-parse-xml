<?php
/**
 * Created by PhpStorm.
 * User: doba
 * Date: 16.11.16
 * Time: 09:02
 */

namespace Fw\Template;


interface TemplateInterface {


	/**
	 * @param       $template
	 * @param array $params
	 *
	 * @return string
	 */
	public function render($template,array $params = array());
}