<?php
/**
 * Created by PhpStorm.
 * User: doba
 * Date: 16.11.16
 * Time: 08:56
 */

namespace Fw\Template;


use Fw\Filesystem\Finder;

class Template implements TemplateInterface {

	/**
	 * @var Finder
	 */
	private $finder;

	private $extension = 'php';

	/**
	 * @param Finder $finder
	 *
	 * @return $this
	 */
	public function setFinder( Finder $finder ) {

		$this->finder = $finder;

		return $this;
	}


	/**
	 * @param       $template
	 * @param array $params
	 *
	 * @return string
	 */
	public function render( $template, array $params = array() ) {

		$file = sprintf( '%s.%s', $template, $this->extension );

		$templateFile = $this->finder->getFile( $file );

		if(0 < count($params)){
			foreach( $params as $key => $value ) {
				$$key = $value;
			}
			unset( $params );
		}

		ob_start();
		require_once $templateFile;
		$template = ob_get_contents();
		ob_end_clean();

		return $template;
	}
}