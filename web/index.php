<?php
/**
 * @var $loader ClassLoader
 */
$loader = require_once __DIR__.'/../app/autoload.php';

$main = new \Fw\Container();
$main->run(new \Book\Web\WebApplication());

