<?php


return array(
	'host'     => 'localhost',
	'username' => 'postgres',
	'password' => '',
	'name'     => '',
	'options'  => array( \PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY )
);