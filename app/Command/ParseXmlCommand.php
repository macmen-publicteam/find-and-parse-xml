<?php
/**
 * Created by PhpStorm.
 * User: doba
 * Date: 12.11.16
 * Time: 13:16
 */

namespace Command;


use Book\Crawler\XmlBookCrawlerFromDirectory;
use Book\Manager\BookManager;
use Book\Model\Book;
use Book\XmlBookBuilder;
use Fw\Command\Input;
use Fw\Command\Output;
use Fw\Command\BaseCommand;

class ParseXmlCommand extends BaseCommand {

	/**
	 * @param Input  $input
	 * @param Output $output
	 *
	 * @return void
	 */
	public function execute( Input $input, Output $output ) {

		if ( ! $input->has( 'path' ) ) {
			throw $this->createParameterNotFoundException( '--path is required parameter' );
		}

		if ( ! $path = realpath( $input->get( 'path' ) ) ) {
			throw $this->createCommandException( sprintf( 'Directory %s Not Found', $input->get( 'path' ) ) );
		}

		$iterator     = new \RecursiveIteratorIterator( new \RecursiveDirectoryIterator( $path, \RecursiveDirectoryIterator::SKIP_DOTS ) );
		$xmlIterator  = new \RegexIterator( $iterator, '/^.+\.xml$/i' );
		$bookBuilder  = new XmlBookBuilder( new XmlBookCrawlerFromDirectory( $xmlIterator ), new Book() );
		$books        = $bookBuilder->getBooks();
		$countBooks   = count( $books );
		$insertedRows = 0;
		$updatedRows  = 0;
		$skipRows     = 0;

		if ( 0 < $countBooks ) {

			/**
			 * @var $bookManager BookManager
			 */
			$bookManager = $this->container->getDatabaseManager( Book::class );

			$now                    = new \DateTime();
			$bookInsertQueryBuilder = $bookManager->bookInsertQueryBuilder()
			                                      ->addConflictFields( array( 'author', 'name' ) )
			                                      ->setUpdateConflictField( 'date', $now->format( Book::DATE_FORMAT ) );
			foreach ( $books as $book ) {
				/**
				 * @var $book Book
				 */
				if ( ! $book->getAuthor() || ! $book->getName() ) {
					$skipRows ++;
					continue;
				}
				if ( $databaseBook = $bookManager->getBookByAuthorAndName( $book->getName(), $book->getAuthor() ) ) {
					$bookManager->updateBook( $databaseBook );
					$updatedRows ++;
					continue;
				}
				$bookManager->insertBookInQuery( $bookInsertQueryBuilder, $book );
				$insertedRows ++;
			}
			if ( $insertedRows > 0 ) {
				try {
					$bookInsertQueryBuilder->execute();
				} catch ( \LogicException $exception ) {
					throw $this->createCommandException( $exception->getMessage() );
				}
			}

		}

		$output->printInfo( sprintf( 'Found Rows: %s, Insert Rows: %s, Update Rows: %s, Skip Rows: %s', $countBooks, $insertedRows, $updatedRows, $skipRows ) );
	}


	/**
	 * @return string
	 */
	public function getName() {
		return 'parse:xml';
	}

	function __toString() {
		return 'Finder Xml And Parse The Files';
	}


}