<?php
/**
 * Created by PhpStorm.
 * User: doba
 * Date: 13.11.16
 * Time: 10:54
 */

namespace Book;


use Book\Model\Book;
use Fw\DomCrawler\DomCrawlerInterface;

class XmlBookBuilder {

	/**
	 * @var DomCrawlerInterface
	 */
	private $crawler;

	/**
	 * @var Book
	 */
	private $bookModel;

	/**
	 * XmlBookBuilder constructor.
	 *
	 * @param DomCrawlerInterface $crawler
	 * @param Book                $book
	 *
	 */
	public function __construct( DomCrawlerInterface $crawler, Book $book ) {

		$this->crawler   = $crawler;
		$this->bookModel = $book;

	}

	/**
	 * @return Book[]
	 */
	public function getBooks() {
		$books = $this->crawler->getItems();
		if ( ! $books ) {
			return array();
		}

		/**
		 * @var $bookModel Model\Book
		 */
		$bookModel = $this->bookModel;

		$p = function ( $row ) use ( $bookModel ) {
			$object = clone $bookModel;
			if ( isset( $row['name'] ) ) {
				$object->setName( $row['name'] );
			}
			if ( isset( $row['author'] ) ) {
				$object->setAuthor( $row['author'] );
			}

			return $object;

		};
		return array_map( $p, $books );

	}


}