<?php
/**
 * Created by PhpStorm.
 * User: doba
 * Date: 12.11.16
 * Time: 19:41
 */

namespace Book\Crawler;


use Fw\DomCrawler\DomCrawlerInterface;
use Fw\DomCrawler\XmlCrawler;

class XmlBookCrawlerFromDirectory extends XmlCrawler implements DomCrawlerInterface {

	/**
	 * @var \Traversable
	 */
	private $iterator;

	public function __construct( \Traversable $iterator ) {
		$this->iterator = $iterator;
	}

	/**
	 * @return array
	 */
	public function getItems() {
		$items = array();
		if ( 0 < iterator_count( $this->iterator ) ) {

			foreach ( $this->iterator as $key => $item ) {

				/**
				 * @var $dom \DOMDocument
				 */
				$dom = $this->getDomDocumentByFile( $item );

				/**
				 * @var $bookElements \DOMNodeList
				 */
				$bookElements = $dom->getElementsByTagName( 'book' );

				if ( 0 === $bookElements->length ) {
					continue;
				}

				foreach ( $bookElements as $bookElement ) {
					$items[] = $this->getTextContentAndConvertDomElementToArray( $bookElement, array( 'author', 'name' ) );
				}

			}
		}

		return $items;
	}

	/**
	 * @return \Traversable
	 */
	public function getIterator() {
		return $this->iterator;
	}
}