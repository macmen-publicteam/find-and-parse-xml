<html>
<head>
	<style>
		.container {
			max-width: 960px;
			margin: 5% auto 0 auto;
		}

		table {
			width: 100%;
			border-collapse: collapse;
			text-align: center;
		}

		tr td, tr th {
			text-align: center;
			padding: 6px 0;
		}
		form input[type='text'] {
			width: 80%;
		}
		h2 {
			text-align: center;
		}
	</style>
</head>
<body>
<div class="container">
	<form method="GET">
		<div>
			<label>
				Find Author <input type="text" name="q" value="<?php echo $query ? $query : '' ?>" />
				<input type="submit" value="Search"/>
			</label>
		</div>
	</form>
	<?php if ( 0 < count( $books ) ) : ?>
		<table>
			<tr>
				<th>Name</th>
				<th>Author</th>
				<th>Date</th>
			</tr>
			<?php foreach ( $books as $book ): /** @var $book \Book\Model\Book */ ?>
				<tr>
					<td><?php echo $book->getName() ?></td>
					<td><?php echo $book->getAuthor() ?></td>
					<td><?php echo $book->getDateToString() ?></td>
				</tr>
			<?php endforeach; ?>
		</table>
	<?php else: ?>
		<h2>Items Not Found</h2>
	<?php endif ?>
</div>
</body>
</html>