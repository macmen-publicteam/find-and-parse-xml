<?php
/**
 * Created by PhpStorm.
 * User: doba
 * Date: 15.11.16
 * Time: 19:05
 */

namespace Book\Web;


use Book\Web\Controller\IndexController;
use Fw\Application\AbstractApplication;
use Fw\Container;
use Fw\Filesystem\Finder;

class WebApplication extends AbstractApplication {

	/**
	 * Execute All Application Staff
	 */
	public function run() {

		// exclude router
		// i just call the controller direly
		$controller = new IndexController();
		echo $controller
			->setContainer($this->container)
			->indexAction();
	}


	public function beforeRun( Container $container ) {
		$finder = new Finder(__DIR__.'/Resources/views');
		$container->getTemplate()->setFinder($finder);
	}
}