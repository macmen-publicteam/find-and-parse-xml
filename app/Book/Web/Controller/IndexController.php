<?php
/**
 * Created by PhpStorm.
 * User: doba
 * Date: 15.11.16
 * Time: 19:07
 */

namespace Book\Web\Controller;


use Book\Manager\BookManager;
use Book\Model\Book;
use Fw\Controller\BaseController;

class IndexController extends BaseController {


	public function indexAction() {

		/**
		 * @var $bookManager BookManager
		 */
		$authorFilter = isset($_GET['q']) ? (string) $_GET['q'] : null;
		$bookManager = $this->getDatabase( Book::class );

		$books = $bookManager->findByAuthor($authorFilter);


		return $this->render('index',array('books' => $books,'query' => $authorFilter));
	}
}