<?php
/**
 * Created by PhpStorm.
 * User: doba
 * Date: 12.11.16
 * Time: 19:32
 */

namespace Book\Model;


use Book\Manager\BookManager;
use Fw\Database\ModelManagerInterface;

/**
 * Class Book
 * @package Book\Model
 * @name books
 */
class Book implements ModelManagerInterface {

	const DATE_FORMAT = 'Y-m-d G:i:s';
	private $id;
	private $author;
	private $name;
	private $date;

	public function __construct() {
		$this->date = new \DateTime();
	}


	/**
	 * @param mixed $id
	 *
	 * @return $this;
	 */
	public function setId( $id ) {
		$this->id = $id;

		return $this;
	}

	/**
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return mixed
	 */
	public function getAuthor() {
		return $this->author;
	}

	/**
	 * @param mixed $author
	 *
	 * @return $this;
	 */
	public function setAuthor( $author ) {
		$this->author = $author;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @param mixed $name
	 *
	 * @return $this;
	 */
	public function setName( $name ) {
		$this->name = $name;

		return $this;
	}

	/**
	 * @return \DateTime
	 */
	public function getDate() {
		return $this->date;
	}


	public function getDateToString() {
		return $this->date->format( self::DATE_FORMAT );
	}

	/**
	 * @param \DateTime $date
	 *
	 * @return $this;
	 */
	public function setDate( \DateTime $date ) {
		$this->date = $date;

		return $this;
	}

	function __clone() {
		$this->setName( null );
		$this->setAuthor( null );
		$this->setDate( new \DateTime() );
	}


	/**
	 * @return string
	 */
	public static function getModelManager() {
		return BookManager::class;
	}
}