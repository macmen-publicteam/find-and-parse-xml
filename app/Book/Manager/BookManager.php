<?php
/**
 * Created by PhpStorm.
 * User: doba
 * Date: 14.11.16
 * Time: 12:06
 */

namespace Book\Manager;


use Book\Model\Book;
use Fw\Database\Exception\ResultNotFoundException;
use Fw\Database\InsertQueryBuilder;
use Fw\Database\Manager;

class BookManager extends Manager {


	protected $fields = array( 'name', 'author', 'date' );

	/**
	 * @return \Fw\Database\InsertQueryBuilder
	 */
	public function bookInsertQueryBuilder() {
		return $this->createInsertQueryBuilder( $this->fields );
	}


	/**
	 * @param InsertQueryBuilder $queryBuilder
	 * @param Book               $book
	 *
	 * @return $this
	 */
	public function insertBookInQuery( InsertQueryBuilder $queryBuilder, Book $book ) {
		$queryBuilder->addValue( array(
			'name'   => $book->getName(),
			'author' => $book->getAuthor(),
			'date'   => $book->getDateToString()
		) );

		return $this;
	}

	/**
	 * @param Book $book
	 *
	 * @return Book
	 */
	public function updateBook( Book $book ) {
		if ( ! $book->getId() ) {
			throw new \LogicException( 'The Book is not Database Instance' );
		}

		$book->setDate( new \DateTime() );
		$object = $this->updateWithAndStatement( array( 'date' => $book->getDateToString() ), array( 'id' => $book->getId() ) );

		return $object;
	}

	/**
	 * @param $name
	 * @param $author
	 *
	 * @return null|object|Book
	 */
	public function getBookByAuthorAndName( $name, $author ) {
		try {
			return $this->getWithAndStatement( array( 'name' => $name, 'author' => $author ) );
		} catch ( ResultNotFoundException $exception ) {
			return null;
		}
	}

	public function findByAuthor( $authorName ) {

		$sql = "SELECT * FROM " . $this->getClassMetaData()->getTableName() . " WHERE LOWER(author) LIKE  LOWER(:author) ORDER BY date DESC;";

		$this
			->connection
			->prepare( $sql )
			->execute( array( ':author' => '%' . $authorName . '%' ) );

		return $this->getResult();
	}


}