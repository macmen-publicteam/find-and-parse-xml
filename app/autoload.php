<?php

require_once __DIR__."/../core/src/ClassLoader.php";

$loader = new ClassLoader();
$loader->addPrefix('Command',__DIR__);
$loader->addPrefix('Book',__DIR__);
$loader->addPrefix('Fw',__DIR__.'/../core/src');
$loader->register();
return $loader;