CREATE TABLE books
(
  id serial NOT NULL,
  name character varying(255),
  author character varying(255),
  date timestamp without time zone,
  CONSTRAINT book_pk PRIMARY KEY (id),
  CONSTRAINT unique_toddler UNIQUE (name, author)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE books
  OWNER TO postgres;


